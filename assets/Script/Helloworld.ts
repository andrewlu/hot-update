const {ccclass, property} = cc._decorator;

@ccclass
export default class Helloworld extends cc.Component {

    onOpenBundle() {
        cc.assetManager.loadBundle("Bundle2", (err, bundle) => {
            if (err) {
                console.error("ERROR", err);
                return;
            }
            bundle.load<cc.Prefab>("UI2", cc.Prefab, (err2, ui2) => {
                const ui = cc.instantiate(ui2);
                this.node.addChild(ui);
            });
        });
    }
}
