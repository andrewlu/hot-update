const {ccclass, property} = cc._decorator;

//@ts-ignore
import UpdateManager = require('UpdateManager');

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    start() {
        if (UpdateManager.getUpdateFlag()) {
            this.label.string = UpdateManager.cur_ver_desc;
            UpdateManager.resetUpdateFlag();
        } else {
            this.label.string = "未发现新版本,\n当前版本:" + UpdateManager.cur_ver_name;
        }
        console.log('>>>>>>>>>>>>>>>>>>>>>', UpdateManager.cur_ver_code);
    }
}
