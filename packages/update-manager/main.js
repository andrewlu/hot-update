'use strict';
const fs = require('fs');
const path = require('path');
const FileUtil = require('./js/FileUtils');
const FileUpload = require('./js/FileUploader');
const logger = require('./js/logger');

// 编译完成仅保存每次生成的bundleVers. 之后注入热更阶段再合并.
const doMakeUpdatePackage = function (opt, cb) {
    if (opt.platform == 'android' || opt.platform == 'ios') {
        try {
            const bundleVers = {};
            for (let b of opt.bundles) {
                bundleVers[b.name] = b.version;
            }
            let targetUpdatePath = path.join(opt.dest, `remote/bundleVers.json`);
            FileUtil.write(targetUpdatePath, JSON.stringify(bundleVers));

            cb && cb();
        } catch (e) {
            logger.log("未生成更新包", e);
            cb && cb();
        }
    } else {
        cb && cb();
    }
};

const getConfigJson = function () {
    const configStr = fs.readFileSync(
        Editor.url('packages://update-manager/.settings.conf'), 'utf-8');
    const configJson = JSON.parse(configStr || "{}");
    if (typeof configJson.versionCfg.versionCode === 'undefined') {
        logger.warn("未配置更新版本,不生成热更Patch");
        return null;
    }
    if (!configJson.versionCfg.versionType || configJson.versionCfg.versionType.length <= 0) {
        configJson.versionCfg.versionType = 'dev';
    }
    return configJson;
};

// 注入热更信息.
const updateHotFixInfo = function () {
    const configJson = getConfigJson();
    if (!configJson) return;

    const bundleVers = path.join(Editor.Project.path, `build/jsb-link/remote/bundleVers.json`);
    if (!fs.existsSync(bundleVers)) {
        logger.warn('未找到编译信息,请先构建项目.');
        return;
    }
    const bundles = JSON.parse(fs.readFileSync(bundleVers, 'utf-8'));

    const data = {
        versionCode: configJson.versionCfg.versionCode,
        versionName: configJson.versionCfg.versionName,
        versionType: configJson.versionCfg.versionType,
        versionLog: configJson.versionCfg.versionLog,
        forceUpdate: configJson.versionCfg.forceUpdate,
        server: `${configJson.versionCfg.baseUrl}${configJson.ftpCfg.rootPath}${configJson.versionCfg.versionType}/${configJson.versionCfg.versionCode}`,
        bundles: bundles
    };

    let targetUpdatePath = path.join(Editor.Project.path, `build/jsb-link/remote/update-${configJson.versionCfg.versionType}.json`);
    FileUtil.write(targetUpdatePath, JSON.stringify(data, null, 2));
};

module.exports = {
    load() {
        Editor.Builder.on('build-finished', doMakeUpdatePackage);
        try {
            fs.readFileSync(
                Editor.url('packages://update-manager/.settings.conf'), 'utf-8');
        } catch (e) {
            fs.writeFileSync(
                Editor.url('packages://update-manager/.settings.conf'), JSON.stringify({}, null, 2), 'utf-8');
        }
        if (!this.ftp) {
            this.ftp = FileUpload();
        }
        this.ftp.setStateListener(this.onUploadStateChange.bind(this));
    },

    onUploadStateChange(state) {
        logger.log('当前任务执行器状态:', state);
        if (!state) {
            this.startUploadTime = new Date().getTime();
            logger.info('开始上传更新包', this.startUploadTime);
        } else {
            logger.success(`所有文件上传完成,耗时:${Math.floor((new Date().getTime() - this.startUploadTime) / 1000)}秒`);
        }
    },

    unload() {
        Editor.Builder.removeListener('build-finished', doMakeUpdatePackage);
        if (this.ftp) {
            this.ftp.destroy();
            this.ftp = null;
        }
    },
    messages: {
        'open'() {
            Editor.Panel.open('update-manager');
        },
        'saveConfig'() {
            // 上传更新包前,更新一次热更描述文件.
            logger.info('生成热更文件...');
            updateHotFixInfo();
            logger.info('准备remote 资源...');
            const remoteDir = path.join(Editor.Project.path, 'build/jsb-link/remote'); // remote 目录.
            const assetDir = path.join(Editor.Project.path, 'build/jsb-link/assets'); // assets目录.
            FileUtil.copy(assetDir, remoteDir);
            logger.info('remote 资源准备完毕,可以开始上传...');
        },
        'upload'(event, dir, dst, options) {
            this.ftp.setOption(options, options.maxThread);
            this.ftp.upload(dir, dst);
        },
        'getProjectPath'(event) {
            event.reply(Editor.Project.path);
        },
        "checkThreads"(event) {
            event.reply(this.ftp.checkThreads());
        },
        'queryThreadStates'(event) {
            event.reply(this.ftp.checkState());
        },
        'restartThread'(event, name) {
            this.ftp.restart(name);
        },
        'uploadStop'() {
            if (this.ftp) {
                this.ftp.destroy();
            }
        }
    },
};