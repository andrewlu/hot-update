window.beforeBoot = function () {

    // console.log("游戏正在启动中.")
    if (window.remoteUrl) {
        const settings = window._CCSettings;
        settings.server = window.remoteUrl;
        //cc.log("远程资源地址:", settings.server);
    }
    let url = window.updateUrl || false;
    if (!url) {
        cc.log("未配置版本更新地址,跳过更新.")
        window.boot();
        return;
    }
    // 游戏启动后再请求更新,避免影响启动速度.
    url += `?_t=${Math.random()}`;
    cc.log("请求更新地址:", url);

    if (window.updateBefore) {
        _get(url).then(resp => {
            if (!resp) {
                window.boot();
                return;
            }
            window.localStorage.setItem('cur_ver_info', resp);
            window.onBooting();
        });
    } else {
        window.onBooting();
        _get(url).then(resp => {
            if (!resp) {
                return;
            }
            window.localStorage.setItem('cur_ver_info', resp);
        });
    }
};
window.onBooting = function () {
    // 请求缓存信息,判断是否需要更新.
    let assetStr = window.localStorage.getItem('cur_ver_info');
    if (!assetStr) {
        window.boot();
    } else {
        console.log("当前版本信息:", assetStr);
        let asset = JSON.parse(assetStr);
        window.mergeVersion(asset);
        window.boot();

        // 判断当前是否有版本更新.
        const lastVer = window.localStorage.getItem('last_ver_code') || asset.versionCode;
        const curVer = asset.versionCode || 0;
        if (lastVer != curVer) {
            window.localStorage.setItem('new_ver_flag', "1");
        } else {
            window.localStorage.setItem('new_ver_flag', "0");
        }
        window.localStorage.setItem('last_ver_code', curVer);
        // 当前版本名称.
        window.localStorage.setItem('cur_ver_name', asset.versionName);
    }
};
window.mergeVersion = function (updateInfo) {
    const settings = window._CCSettings;
    const bundleVers = updateInfo.bundles;
    settings.server = updateInfo.server;
    if (bundleVers) {
        for (let b in bundleVers) {
            if (bundleVers[b] != settings.bundleVers[b]) {
                // 配置中的bundleVer版本不一致,则添加到remote列表中去,以供远程加载.
                if (settings.remoteBundles.indexOf(b) < 0) {
                    settings.remoteBundles.push(b);
                    console.log('bundle 有更新:', b, bundleVers[b]);
                }
            }
        }
        settings.bundleVers = bundleVers;
    }
};

function _get(url) {
    return new Promise(resolve => {
        const ajax = new XMLHttpRequest();
        ajax.open("get", url, true);
        ajax.setRequestHeader("Content-Type", "application/json;charset=utf-8");
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4) {
                if (ajax.status == 200) {
                    resolve(ajax.responseText);
                } else {
                    resolve(null);
                }
            }
        }
        ajax.send(null);
    });
};